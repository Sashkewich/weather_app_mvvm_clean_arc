package com.example.weatherapp.details_of_day

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.example.weatherapp.R
import com.example.weatherapp.common.mvp.BaseFragment
import com.example.weatherapp.databinding.FragmentDetailsOfDayBinding
import com.example.weatherapp.five_days_weather.model.City
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherData
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherList
import com.example.weatherapp.five_days_weather.model.Weather
import com.example.weatherapp.main_page.model.WeatherData
import com.example.weatherapp.utils.Arguments
import com.example.weatherapp.utils.extensions.args
import com.example.weatherapp.utils.extensions.dateFormat
import com.example.weatherapp.utils.extensions.parseFormatSunriseTime
import com.example.weatherapp.utils.extensions.parseFormatSunsetTime
import com.example.weatherapp.utils.extensions.viewbinding.viewBinding

class DetailsOfDayFragment : BaseFragment(R.layout.fragment_details_of_day) {

    companion object {
        fun newInstance(data: FiveDaysWeatherList) = DetailsOfDayFragment().apply {
            arguments = bundleOf(Arguments.FIVE_DAYS_WEATHER_LIST to data)
        }
    }

    private val dataList: FiveDaysWeatherList? by lazy {
        arguments?.getParcelable(Arguments.FIVE_DAYS_WEATHER_LIST)
    }

    private val binding:FragmentDetailsOfDayBinding by viewBinding()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding){
            toolbarCityName.text =
                "${dataList?.let { dateFormat(it) }}"
            tempValue.text = "${dataList?.main?.temp?.toInt()}°С"
            weatherSmallText.text =
                dataList?.weather?.get(0)?.description?.replaceFirstChar { it.uppercase() }
//            weatherData?.let { changeIcon(it) }
            windSpeedValue.text = "${dataList?.wind?.speed?.toInt().toString()} м/с"
            humidityValue.text = "${dataList?.main?.humidity?.toString()} %"
            visibilityValue.text = "${dataList?.visibility?.div(1000).toString()} км"
//            sunriseValue.text = weatherData?.let { parseFormatSunriseTime(it) }
//            sunsetValue.text = weatherData?.let { parseFormatSunsetTime(it) }
//            sunsetValue.text = weatherData.toString()
            backButton.setOnClickListener {
                val fragmentManager = parentFragmentManager
                fragmentManager.popBackStack()
            }
        }
    }

//    fun changeIcon(data: FiveDaysWeatherData) {
//        with(binding) {
//            when (data.list?.get(0)?.weather?.get(0)?.description) {
//                "ясно" -> weatherIcon.setImageResource(R.drawable.weather_sunny_icon)
//
//                "небольшая облачность" -> weatherIcon.setImageResource(R.drawable.weather_few_clouds_icon)
//
//                "scattered clouds", "broken clouds", "overcast clouds" -> weatherIcon.setImageResource(
//                    R.drawable.overcast_clouds_icon
//                )
//
//                "light rain" -> weatherIcon.setImageResource(R.drawable.light_rain_icon)
//
//                "moderate rain", "heavy intensity rain", "very heavy rain", "extreme rain", "freezing rain", "light intensity shower rain",
//                "shower rain", "heavy intensity shower rain", "ragged shower rain" -> weatherIcon.setImageResource(
//                    R.drawable.rain_icon
//                )
//
//                "thunderstorm with light rain", "thunderstorm with rain", "thunderstorm with heavy rain", "light thunderstorm",
//                "thunderstorm", "heavy thunderstorm", "ragged thunderstorm", "thunderstorm with light drizzle", "thunderstorm with drizzle",
//                "thunderstorm with heavy drizzle" -> weatherIcon.setImageResource(R.drawable.thunderstorm_icon)
//
//                "light shower snow", "light snow", "snow", "heavy snow", "sleet", "light shower sleet", "shower sleet",
//                "light rain and snow", "rain and snow", "shower snow", "heavy shower snow" -> weatherIcon.setImageResource(
//                    R.drawable.snow_icon
//                )
//
//                "mist", "fog" -> weatherIcon.setImageResource(R.drawable.mist_icon)
//
//                "light intensity drizzle", "drizzle", "heavy intensity drizzle", "light intensity drizzle rain",
//                "drizzle rain", "heavy intensity drizzle rain", "shower rain and drizzle", "heavy shower rain and drizzle",
//                "shower drizzle" -> weatherIcon.setImageResource(R.drawable.cloud_drizzle_icon)
//            }
//        }
//    }
}