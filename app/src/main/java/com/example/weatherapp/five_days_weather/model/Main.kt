package com.example.weatherapp.five_days_weather.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Main(
    val temp: Float?,
    val humidity: Int?
) : Parcelable