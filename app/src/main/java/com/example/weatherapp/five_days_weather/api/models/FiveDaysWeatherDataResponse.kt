package com.example.weatherapp.five_days_weather.api.models

data class FiveDaysWeatherDataResponse(
    var list: List<FiveDaysWeatherListResponse>?,
    var city: CityResponse?,
)