package com.example.weatherapp.five_days_weather.ui

import com.example.weatherapp.common.mvp.MvpPresenter
import com.example.weatherapp.common.mvp.MvpView
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherList

interface FiveDaysWeatherContract {
    interface View : MvpView {
        fun showLoading(isLoading: Boolean)
        fun showError(error: String)
        fun showData(data: List<FiveDaysWeatherList>)
    }

    interface Presenter : MvpPresenter<View> {
        fun getWeatherData(cityName: String, lang: String, key: String, units: String)
    }
}