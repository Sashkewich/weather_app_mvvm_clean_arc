package com.example.weatherapp.five_days_weather.di

import com.example.weatherapp.common.di.InjectionModule
import com.example.weatherapp.five_days_weather.api.FiveDaysWeatherApi
import com.example.weatherapp.five_days_weather.interactor.FiveDaysWeatherInteractor
import com.example.weatherapp.five_days_weather.repository.FiveDaysWeatherRemoteRepository
import com.example.weatherapp.five_days_weather.repository.FiveDaysWeatherRepository
import com.example.weatherapp.five_days_weather.ui.FiveDaysWeatherPresenter
import com.example.weatherapp.five_days_weather.ui.FiveDaysWeatherViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object FiveDaysModule : InjectionModule {
    override fun create() = module {
        single { get<Retrofit>().create(FiveDaysWeatherApi::class.java) }
        single<FiveDaysWeatherRepository> { FiveDaysWeatherRemoteRepository(get()) }
        single { FiveDaysWeatherRemoteRepository(get()) } bind FiveDaysWeatherRepository::class
        factoryOf(::FiveDaysWeatherInteractor)

        viewModel { FiveDaysWeatherPresenter(get()) }
        viewModel { FiveDaysWeatherViewModel(get()) }
    }
}