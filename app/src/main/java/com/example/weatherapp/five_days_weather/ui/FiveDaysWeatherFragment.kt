package com.example.weatherapp.five_days_weather.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.R
import com.example.weatherapp.common.mvp.BaseFragment
import com.example.weatherapp.databinding.FragmentFiveDaysBinding
import com.example.weatherapp.details_of_day.DetailsOfDayFragment
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherList
import com.example.weatherapp.five_days_weather.ui.adapter.FiveDaysWeatherAdapter
import com.example.weatherapp.utils.Arguments
import com.example.weatherapp.utils.extensions.args
import com.example.weatherapp.utils.extensions.popScreen
import com.example.weatherapp.utils.extensions.replace
import com.example.weatherapp.utils.extensions.viewbinding.viewBinding
import com.example.weatherapp.utils.extensions.withArgs
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

private const val KEY = "2cb6cb1fd4ebd0d67addeeb82e919110"
private const val LANG = "ru"
private const val UNITS = "metric"

class FiveDaysWeatherFragment : BaseFragment(R.layout.fragment_five_days) {
    private val binding: FragmentFiveDaysBinding by viewBinding()
    private val viewModel: FiveDaysWeatherViewModel by viewModel()

    companion object {
        fun newInstance(cityName: String) = FiveDaysWeatherFragment()
            .withArgs(Arguments.CITY_NAME to cityName)
    }

    private val cityName: String? by args(Arguments.CITY_NAME)

    private val adapter: FiveDaysWeatherAdapter by lazy {
        FiveDaysWeatherAdapter { item ->
            replace(DetailsOfDayFragment.newInstance(item))
        }
    }
    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(context)
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showLoading(true)
        viewModel.fiveDaysWeatherLiveData.observe(viewLifecycleOwner) { fiveDaysWeatherList ->
            if (fiveDaysWeatherList != null) {
                showData(fiveDaysWeatherList)
                showLoading(false)
            }
        }

        viewModel.getWeatherData(cityName.toString(), LANG, KEY, UNITS) // presenter.getWeatherData(city = cityName.toString())

        with(binding) {
            toolbarCityName.text = cityName
            recyclerViewFiveDaysWeather.adapter = adapter
            recyclerViewFiveDaysWeather.layoutManager = layoutManager
            recyclerViewFiveDaysWeather.setHasFixedSize(true)
            adapter.onAttachedToRecyclerView(recyclerViewFiveDaysWeather)

            toolbar.setNavigationOnClickListener {
                popScreen()
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progress.isVisible = isLoading
    }

    private fun showError(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
        Timber.i("ERROR --->>> $error")
    }

    private fun showData(data: List<FiveDaysWeatherList>) {
        adapter.setData(data)
    }
}