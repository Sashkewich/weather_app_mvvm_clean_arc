package com.example.weatherapp.five_days_weather.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FiveDaysWeatherList(
    val dt: Long?,
    val main: Main?,
    val weather: List<Weather>?,
    val wind: Wind?,
    val visibility: Int?,
    val dtTxt: String?
) : Parcelable