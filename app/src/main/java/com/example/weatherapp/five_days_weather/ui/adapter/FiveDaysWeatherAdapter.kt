package com.example.weatherapp.five_days_weather.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherList

class FiveDaysWeatherAdapter(
    private val clickOnDay: (FiveDaysWeatherList) -> Unit
) : RecyclerView.Adapter<FiveDaysWeatherViewHolder>() {

    val data = mutableListOf<FiveDaysWeatherList>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : FiveDaysWeatherViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_weather, parent, false)
        return FiveDaysWeatherViewHolder(parent,clickOnDay)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: FiveDaysWeatherViewHolder, position: Int) {
        val listItem = data[position]
        holder.onBind(listItem)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(items: List<FiveDaysWeatherList>){
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }
}