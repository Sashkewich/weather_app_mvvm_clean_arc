package com.example.weatherapp.five_days_weather.ui.adapter

enum class EnumWeatherDescriptionType(val description: String) {
    CLEAR_SKY("ясно"), // clear sky
    FEW_CLOUDS("небольшая облачность"), // few clouds
    SCATTERED_CLOUDS("переменная облачность"),
    BROKEN_CLOUDS("облачно с прояснениями"),
    OVERCAST_CLOUDS("пасмурно"), // overcast clouds
    LIGHT_RAIN("небольшой дождь"), // light rain
    MODERATE_RAIN("дождь"), // moderate rain
    HEAVY_INTENSITY_RAIN("heavy intensity rain"), // heavy intensity rain
    VERY_HEAVY_RAIN("very heavy rain"),
    EXTREME_RAIN("extreme rain"),
    FREEZING_RAIN("freezing rain"),
    LIGHT_INTENSITY_SHOWER_RAIN("небольшой проливной дождь"), // light intensity shower rain
    SHOWER_RAIN("shower rain"),
    HEAVY_INTENSITY_SHOWER_RAIN("heavy intensity shower rain"),
    RAGGED_SHOWER_RAIN("ragged shower rain"),
    THUNDERSTORM_WITH_LIGHT_RAIN("thunderstorm with light rain"),
    THUNDERSTORM_WITH_RAIN("thunderstorm with rain"),
    THUNDERSTORM_WITH_HEAVY_RAIN("thunderstorm with heavy rain"),
    LIGHT_THUNDERSTORM("light thunderstorm"),
    THUNDERSTORM("thunderstorm"),
    HEAVY_THUNDERSTORM("heavy thunderstorm"),
    RAGGED_THUNDERSTORM("ragged thunderstorm"),
    THUNDERSTORM_WITH_LIGHT_DRIZZLE("thunderstorm with light drizzle"),
    THUNDERSTORM_WITH_DRIZZLE("thunderstorm with drizzle"),
    THUNDERSTORM_WITH_HEAVY_DRIZZLE("thunderstorm with heavy drizzle"),
    LIGHT_SHOWER_SNOW("light shower snow"),
    LIGHT_SNOW("небольшой снег"), // light snow
    SNOW("снег"), // snow
    HEAVY_SNOW("heavy snow"),
    SLEET("sleet"),
    LIGHT_SHOWER_SLEET("light shower sleet"),
    SHOWER_SLEET("shower sleet"),
    LIGHT_RAIN_AND_SNOW("light rain and snow"),
    RAIN_AND_SNOW("rain and snow"),
    SHOWER_SNOW("light shower snow"),
    HEAVY_SHOWER_SNOW("heavy shower snow"),
    MIST("mist"),
    FOG("fog"),
    LIGHT_INTENSITY_DRIZZLE("light intensity drizzle"),
    DRIZZLE("drizzle"),
    HEAVY_INTENSITY_DRIZZLE("heavy intensity drizzle"),
    LIGHT_INTENSITY_DRIZZLE_RAIN("light intensity drizzle rain"),
    DRIZZLE_RAIN("drizzle rain"),
    HEAVY_INTENSITY_DRIZZLE_RAIN("heavy intensity drizzle rain"),
    SHOWER_RAIN_AND_DRIZZLE("shower rain and drizzle"),
    HEAVY_SHOWER_RAIN_AND_DRIZZLE("heavy shower rain and drizzle"),
    SHOWER_DRIZZLE("shower drizzle")
}