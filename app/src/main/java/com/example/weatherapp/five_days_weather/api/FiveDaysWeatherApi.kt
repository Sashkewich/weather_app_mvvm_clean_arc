package com.example.weatherapp.five_days_weather.api

import com.example.weatherapp.five_days_weather.api.models.FiveDaysWeatherDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface FiveDaysWeatherApi {
    @GET("data/2.5/forecast") // ?lang=ru&appid=2cb6cb1fd4ebd0d67addeeb82e919110&units=metric
    suspend fun getFiveDaysWeatherData(
        @Query("q") cityName: String,
        @Query("lang") lang: String,
        @Query("appid") key: String,
        @Query("units") units: String
    ) : FiveDaysWeatherDataResponse
}