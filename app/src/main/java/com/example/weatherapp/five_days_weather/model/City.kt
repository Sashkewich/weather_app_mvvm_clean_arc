package com.example.weatherapp.five_days_weather.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class City(
    var name: String?,
    var country: String?,
    var sunrise: Long?,
    var sunset: Long?
) : Parcelable