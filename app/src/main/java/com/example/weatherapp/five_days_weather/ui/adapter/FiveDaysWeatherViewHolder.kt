package com.example.weatherapp.five_days_weather.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.databinding.ItemWeatherBinding
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherList
import com.example.weatherapp.utils.extensions.dateFormat
import com.example.weatherapp.utils.extensions.getDrawableIcon
import java.text.SimpleDateFormat
import java.util.*

class FiveDaysWeatherViewHolder(
    private val binding: ItemWeatherBinding,
    private val clickOnDay: (FiveDaysWeatherList) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    constructor(
        parent: ViewGroup,
        clickOnDay: (FiveDaysWeatherList) -> Unit
    ) : this(
        ItemWeatherBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        clickOnDay
    )

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun onBind(item: FiveDaysWeatherList) {
        val description = item.weather?.get(0)?.description.toString()

        with(binding) {
            timeValue.text = dateFormat(item)
            tempValue.text = "${item.main?.temp?.toInt()}°C"
            windSpeedValue.text = "${item.wind?.speed?.toInt()} м/с"
            humidityValue.text = "${item.main?.humidity}%"
            textWeatherValue.text = "${item.weather?.get(0)?.description}"
            weatherIcon.setImageResource(getDrawableIcon(description))
        }
        itemView.setOnClickListener {
            clickOnDay(item)
        }
    }
}