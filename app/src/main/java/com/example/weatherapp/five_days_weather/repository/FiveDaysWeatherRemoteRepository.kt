package com.example.weatherapp.five_days_weather.repository

import com.example.weatherapp.five_days_weather.api.FiveDaysWeatherApi
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherData
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherConverter

class FiveDaysWeatherRemoteRepository(val api: FiveDaysWeatherApi) : FiveDaysWeatherRepository {

    override suspend fun getFiveDaysWeatherData(
        cityName: String, lang: String, key: String, units: String
    ): FiveDaysWeatherData {
        val data = api.getFiveDaysWeatherData(cityName, lang, key, units)
        return FiveDaysWeatherConverter.fromNetwork(data)
    }
}