package com.example.weatherapp.five_days_weather.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherapp.five_days_weather.interactor.FiveDaysWeatherInteractor
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherData
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherList
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import timber.log.Timber

class FiveDaysWeatherViewModel(private val interactor: FiveDaysWeatherInteractor) : ViewModel() {
    val fiveDaysWeatherLiveData = MutableLiveData<List<FiveDaysWeatherList>?>()
    val isLoading = MutableLiveData<Boolean>()

    fun getWeatherData(cityName: String, lang: String, key: String, units: String) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val fiveDaysWeatherData = interactor.getWeatherData(cityName, lang, key, units)
                val fiveDaysWeatherLiveDataList = fiveDaysWeatherData.list
                fiveDaysWeatherLiveData.value = fiveDaysWeatherLiveDataList
            } catch (t: Throwable) {
                Timber.e(t.message)
            } catch (e: CancellationException) {
                Timber.e(e.message)
            } finally {
                isLoading.value = false
            }
        }
    }
}