package com.example.weatherapp.five_days_weather.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Weather(
    val main: String?,
    val description: String?
) : Parcelable