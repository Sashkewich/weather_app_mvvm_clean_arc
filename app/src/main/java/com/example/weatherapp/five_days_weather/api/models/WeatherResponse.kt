package com.example.weatherapp.five_days_weather.api.models

data class WeatherResponse(
    val main: String?,
    val description: String?
)