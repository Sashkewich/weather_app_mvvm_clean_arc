package com.example.weatherapp.five_days_weather.api.models

data class WindResponse(
    val speed: Float?
)