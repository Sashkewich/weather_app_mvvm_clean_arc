package com.example.weatherapp.five_days_weather.api.models

data class CityResponse(
    var name: String?,
    var country: String?,
    var sunrise: Long?,
    var sunset: Long?
)