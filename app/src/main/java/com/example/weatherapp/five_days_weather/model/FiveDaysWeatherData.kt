package com.example.weatherapp.five_days_weather.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FiveDaysWeatherData(
    var list: List<FiveDaysWeatherList>?,
    var city: City?,
) : Parcelable