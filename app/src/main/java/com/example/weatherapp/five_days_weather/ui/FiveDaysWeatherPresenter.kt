package com.example.weatherapp.five_days_weather.ui

import com.example.weatherapp.common.mvp.BasePresenter
import com.example.weatherapp.five_days_weather.interactor.FiveDaysWeatherInteractor
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import timber.log.Timber


class FiveDaysWeatherPresenter(
    private val interactor: FiveDaysWeatherInteractor
) : BasePresenter<FiveDaysWeatherContract.View>(), FiveDaysWeatherContract.Presenter {

    override fun getWeatherData(cityName: String, lang: String, key: String, units: String) {
        launch {
            try {
                view?.showLoading(true)
                val weatherData = interactor.getWeatherData(cityName, lang, key, units)
                val data = weatherData.list
                data.let {
                    if (it != null) {
                        view?.showData(it)
                    }
                }

            } catch (t: Throwable) {
                view?.showError(t.message.toString())
            } catch (e: CancellationException) {
                Timber.d("Error coroutines ${e.message}")
            } finally {
                view?.showLoading(false)
            }
        }
    }
}