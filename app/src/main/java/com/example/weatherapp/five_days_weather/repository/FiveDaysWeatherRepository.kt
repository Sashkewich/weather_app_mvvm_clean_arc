package com.example.weatherapp.five_days_weather.repository

import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherData

interface FiveDaysWeatherRepository {
    suspend fun getFiveDaysWeatherData(
        cityName: String, lang: String, key: String, units: String
    ): FiveDaysWeatherData?
}