package com.example.weatherapp.five_days_weather.api.models

data class FiveDaysWeatherListResponse(
    val dt: Long?,
    val main: MainResponse?,
    val weather: List<WeatherResponse>?,
    val wind: WindResponse?,
    val visibility: Int?,
    val dtTxt: String?
)