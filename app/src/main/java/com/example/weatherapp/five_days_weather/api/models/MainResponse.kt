package com.example.weatherapp.five_days_weather.api.models

data class MainResponse(
    val temp: Float?,
    val humidity: Int?
)