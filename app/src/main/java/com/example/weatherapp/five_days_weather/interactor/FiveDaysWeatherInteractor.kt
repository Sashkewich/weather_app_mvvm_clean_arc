package com.example.weatherapp.five_days_weather.interactor

import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherData
import com.example.weatherapp.five_days_weather.repository.FiveDaysWeatherRemoteRepository

class FiveDaysWeatherInteractor(private val remoteRepository: FiveDaysWeatherRemoteRepository) {
    suspend fun getWeatherData(cityName: String, lang: String, key: String, units: String): FiveDaysWeatherData {
        return remoteRepository.getFiveDaysWeatherData(cityName, lang, key, units)
    }
}