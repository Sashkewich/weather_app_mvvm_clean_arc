package com.example.weatherapp.five_days_weather.model

import com.example.weatherapp.five_days_weather.api.models.*

object FiveDaysWeatherConverter {
    fun fromNetwork(response: FiveDaysWeatherDataResponse): FiveDaysWeatherData {
        return FiveDaysWeatherData(
            city = response.city?.let { fromNetwork(it)},
            list = response.list?.let { fromNetwork(it)})
    }

    private fun fromNetwork(response: List<FiveDaysWeatherListResponse>): List<FiveDaysWeatherList> {
        return response.map { data ->
            FiveDaysWeatherList(
                dt = data.dt,
                main = data.main?.let { fromNetwork(it) },
                wind = data.wind?.let { fromNetwork(it) },
                visibility = data.visibility,
                weather = data.weather?.let { fromNetworkWeather(it) },
                dtTxt = data.dtTxt
            )
        }
    }

    private fun fromNetwork(response: MainResponse): Main {
        return Main(
            temp = response.temp,
            humidity = response.humidity,
        )
    }

    private fun fromNetwork(response: WindResponse): Wind {
        return Wind(
            speed = response.speed
        )
    }

    private fun fromNetworkWeather(response: List<WeatherResponse>): List<Weather> {
        return response.map { data ->
            Weather(
                main = data.main,
                description = data.description
            )
        }
    }

    private fun fromNetwork(response: CityResponse): City {
        return City(
            name = response.name,
            country = response.country,
            sunrise = response.sunrise,
            sunset = response.sunset
        )
    }
}