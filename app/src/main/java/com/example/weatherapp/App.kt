package com.example.weatherapp

import android.app.Application
import com.example.weatherapp.common.di.NetworkModule
import com.example.weatherapp.five_days_weather.di.FiveDaysModule
import com.example.weatherapp.main_page.di.MainPageModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.context.GlobalContext.stopKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        setupKoin()
    }

    private fun setupTimber() {
        Timber.plant(Timber.DebugTree())
    }

    private fun setupKoin() {
        stopKoin()
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    NetworkModule.create(),
                    MainPageModule.create(),
                    FiveDaysModule.create()
                )
            )
        }
    }
}