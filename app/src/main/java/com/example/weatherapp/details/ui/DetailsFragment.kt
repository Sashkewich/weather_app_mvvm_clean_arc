package com.example.weatherapp.details.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import com.example.weatherapp.R
import com.example.weatherapp.common.mvp.BaseFragment
import com.example.weatherapp.databinding.FragmentDetailsBinding
import com.example.weatherapp.five_days_weather.ui.FiveDaysWeatherFragment
import com.example.weatherapp.main_page.model.WeatherData
import com.example.weatherapp.utils.extensions.args
import com.example.weatherapp.utils.extensions.withArgs
import com.example.weatherapp.utils.Arguments
import com.example.weatherapp.utils.extensions.replace
import com.example.weatherapp.utils.extensions.viewbinding.viewBinding
import java.text.SimpleDateFormat

class DetailsFragment : BaseFragment(R.layout.fragment_details){
    private val binding: FragmentDetailsBinding by viewBinding()

    @SuppressLint("SimpleDateFormat")
    private val parseFormat = SimpleDateFormat("HH:mm:ss")

    companion object {
        fun newInstance(weatherData: WeatherData?) = DetailsFragment()
            .withArgs(Arguments.WEATHER_DATA to weatherData)
    }

    private val weatherData: WeatherData? by args(Arguments.WEATHER_DATA)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showDetailedWeather()

        with(binding) {
            buttonGetWeatherOn5Days.isVisible = false
            backButton.setOnClickListener {
                val fragmentManager = parentFragmentManager
                fragmentManager.popBackStack()
            }
            buttonGetWeatherOn5Days.setOnClickListener {
                val city = toolbarCityName.text.toString()
                replace(FiveDaysWeatherFragment.newInstance(city))

            }
            buttonGetWeatherOn5Days.isVisible = true
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showDetailedWeather() {
        with(binding) {
            toolbarCityName.text =
                "${weatherData?.name?.replaceFirstChar { it.uppercase() }}"
            tempValue.text = "${weatherData?.main?.temp?.minus(273.15)?.toInt()}°С"
            weatherSmallText.text =
                weatherData?.weather?.get(0)?.description?.replaceFirstChar { it.uppercase() }
            weatherData?.let { changeIcon(it) }
            windSpeedValue.text = "${weatherData?.wind?.speed?.toInt().toString()} м/с"
            humidityValue.text = "${weatherData?.main?.humidity?.toString()} %"
            visibilityValue.text = "${weatherData?.visibility?.div(1000).toString()} км"
            sunriseValue.text = parseFormat.format(weatherData?.sys?.sunrise?.times(1000))
            sunsetValue.text = parseFormat.format(weatherData?.sys?.sunset?.times(1000))
        }
    }

    fun changeIcon(data: WeatherData) {
        with(binding) {
            when (data.weather?.get(0)?.description.toString()) {
                "clear sky" -> weatherIcon.setImageResource(R.drawable.weather_sunny_icon)

                "few clouds" -> weatherIcon.setImageResource(R.drawable.weather_few_clouds_icon)

                "scattered clouds", "broken clouds", "overcast clouds" -> weatherIcon.setImageResource(
                    R.drawable.overcast_clouds_icon
                )

                "light rain" -> weatherIcon.setImageResource(R.drawable.light_rain_icon)

                "moderate rain", "heavy intensity rain", "very heavy rain", "extreme rain", "freezing rain", "light intensity shower rain",
                "shower rain", "heavy intensity shower rain", "ragged shower rain" -> weatherIcon.setImageResource(
                    R.drawable.rain_icon
                )

                "thunderstorm with light rain", "thunderstorm with rain", "thunderstorm with heavy rain", "light thunderstorm",
                "thunderstorm", "heavy thunderstorm", "ragged thunderstorm", "thunderstorm with light drizzle", "thunderstorm with drizzle",
                "thunderstorm with heavy drizzle" -> weatherIcon.setImageResource(R.drawable.thunderstorm_icon)

                "light shower snow", "light snow", "snow", "heavy snow", "sleet", "light shower sleet", "shower sleet",
                "light rain and snow", "rain and snow", "shower snow", "heavy shower snow" -> weatherIcon.setImageResource(
                    R.drawable.snow_icon
                )

                "mist", "fog" -> weatherIcon.setImageResource(R.drawable.mist_icon)

                "light intensity drizzle", "drizzle", "heavy intensity drizzle", "light intensity drizzle rain",
                "drizzle rain", "heavy intensity drizzle rain", "shower rain and drizzle", "heavy shower rain and drizzle",
                "shower drizzle" -> weatherIcon.setImageResource(R.drawable.cloud_drizzle_icon)
            }
        }
    }
}