package com.example.weatherapp.utils

object Arguments {
    const val CITY_NAME = "CITY_NAME"
    const val WEATHER_DATA = "WEATHER_DATA"
    const val FIVE_DAYS_WEATHER_LIST = "FIVE_DAYS_WEATHER_LIST"
}