package com.example.weatherapp.utils.extensions

import androidx.fragment.app.*
import androidx.lifecycle.Lifecycle
import com.example.weatherapp.R

private const val PREVIOUS_FRAGMENT_TAG_ARG = "PREVIOUS_FRAGMENT_TAG_ARG"

fun Fragment.replace(fragment: Fragment) {
    val fragmentManager = requireActivity().supportFragmentManager
    val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
    fragmentTransaction.addToBackStack(null)
        .replace(R.id.container, fragment)
        .commit()
}

fun Fragment.popScreen() {
    requireActivity().hideKeyboard()

    val fragmentManager = parentFragment?.childFragmentManager ?: childFragmentManager
    if (fragmentManager.backStackEntryCount < 2) {
        requireActivity().popFeature()
    } else {
        whenStateAtLeast(Lifecycle.State.STARTED) { fragmentManager.popBackStack() }
    }
}

fun FragmentActivity.popFeature() {
    if (supportFragmentManager.backStackEntryCount < 2) {
        finish()
    } else {
        whenStateAtLeast(Lifecycle.State.STARTED) { supportFragmentManager.popBackStack() }
    }
}

private fun Fragment.getPreviousTag(): String? = arguments?.getString(PREVIOUS_FRAGMENT_TAG_ARG)


fun Fragment.getCurrentScreen(): Fragment? =
    childFragmentManager.findFragmentById(R.id.container)