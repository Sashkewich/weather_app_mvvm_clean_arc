package com.example.weatherapp.utils.extensions

import android.annotation.SuppressLint
import com.example.weatherapp.R
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherData
import com.example.weatherapp.five_days_weather.model.FiveDaysWeatherList
import com.example.weatherapp.five_days_weather.ui.adapter.EnumWeatherDescriptionType
import java.text.SimpleDateFormat

@SuppressLint("SimpleDateFormat")
fun dateFormat(item: FiveDaysWeatherList): String {
    val simpleDateFormat =
        SimpleDateFormat("dd MMMM, HH:mm") // конвертация даты в формат (xx.yyyy, чч.мм)
    val dt = item.dt // получаем дату в виде Long (Unix)
    return simpleDateFormat.format(dt?.times(1000L)) // возвращаем дату в виде "дд.мммм, чч:мм"
}

@SuppressLint("SimpleDateFormat")
fun parseFormatSunriseTime(dateTime: FiveDaysWeatherData): String {
    val parseFormat = SimpleDateFormat("HH:mm:ss")
    val sunrise = dateTime.city?.sunrise
    return parseFormat.format(sunrise!!.times(1000L))
}

@SuppressLint("SimpleDateFormat")
fun parseFormatSunsetTime(dateTime: FiveDaysWeatherData): String {
    val parseFormat = SimpleDateFormat("HH:mm:ss")
    val sunset = dateTime.city?.sunset
    return parseFormat.format(sunset!!.times(1000L))
}


//TODO: уберешь из этого метода вариант else ->
// на каждый ENUM тип найди иконку в таком стиле, что я создал и подставь их в формате xml.
// Если будут вопросы пиши. Иконки брал из Material Google Icons ->
// https://fonts.google.com/icons?selected=Material+Symbols+Outlined:rainy:FILL@0;wght@400;GRAD@0;opsz@48&icon.query=weather&icon.platform=android

fun getDrawableIcon(type: String): Int =
    when (type) {
        EnumWeatherDescriptionType.CLEAR_SKY.description -> R.drawable.weather_sunny_icon
        EnumWeatherDescriptionType.FEW_CLOUDS.description -> R.drawable.weather_few_clouds_icon
        EnumWeatherDescriptionType.SCATTERED_CLOUDS.description -> R.drawable.cloud_icon
        EnumWeatherDescriptionType.BROKEN_CLOUDS.description -> R.drawable.weather_few_clouds_icon
        EnumWeatherDescriptionType.OVERCAST_CLOUDS.description -> R.drawable.overcast_clouds_icon

        EnumWeatherDescriptionType.LIGHT_INTENSITY_SHOWER_RAIN.description,
        EnumWeatherDescriptionType.LIGHT_RAIN.description -> R.drawable.light_rain_icon

        EnumWeatherDescriptionType.MODERATE_RAIN.description,
        EnumWeatherDescriptionType.HEAVY_INTENSITY_RAIN.description,
        EnumWeatherDescriptionType.VERY_HEAVY_RAIN.description,
        EnumWeatherDescriptionType.SHOWER_RAIN.description,
        EnumWeatherDescriptionType.RAGGED_SHOWER_RAIN.description,
        EnumWeatherDescriptionType.HEAVY_INTENSITY_SHOWER_RAIN.description,
        EnumWeatherDescriptionType.EXTREME_RAIN.description -> R.drawable.rain_icon

        EnumWeatherDescriptionType.FREEZING_RAIN.description -> R.drawable.rain_icon

        EnumWeatherDescriptionType.THUNDERSTORM_WITH_LIGHT_RAIN.description,
        EnumWeatherDescriptionType.THUNDERSTORM_WITH_RAIN.description,
        EnumWeatherDescriptionType.THUNDERSTORM_WITH_HEAVY_RAIN.description,
        EnumWeatherDescriptionType.LIGHT_THUNDERSTORM.description,
        EnumWeatherDescriptionType.THUNDERSTORM.description,
        EnumWeatherDescriptionType.HEAVY_THUNDERSTORM.description,
        EnumWeatherDescriptionType.RAGGED_THUNDERSTORM.description,
        EnumWeatherDescriptionType.THUNDERSTORM_WITH_LIGHT_DRIZZLE.description,
        EnumWeatherDescriptionType.THUNDERSTORM_WITH_DRIZZLE.description,
        EnumWeatherDescriptionType.THUNDERSTORM_WITH_HEAVY_DRIZZLE.description -> R.drawable.thunderstorm_icon

        EnumWeatherDescriptionType.LIGHT_SHOWER_SNOW.description,
        EnumWeatherDescriptionType.LIGHT_SNOW.description,
        EnumWeatherDescriptionType.SNOW.description,
        EnumWeatherDescriptionType.HEAVY_SNOW.description,
        EnumWeatherDescriptionType.SLEET.description,
        EnumWeatherDescriptionType.LIGHT_SHOWER_SLEET.description,
        EnumWeatherDescriptionType.SHOWER_SLEET.description -> R.drawable.snow_icon

        EnumWeatherDescriptionType.LIGHT_RAIN_AND_SNOW.description,
        EnumWeatherDescriptionType.RAIN_AND_SNOW.description,
        EnumWeatherDescriptionType.SHOWER_SNOW.description,
        EnumWeatherDescriptionType.HEAVY_SHOWER_SNOW.description -> R.drawable.snow_icon

        EnumWeatherDescriptionType.MIST.description -> R.drawable.mist_icon
        EnumWeatherDescriptionType.FOG.description -> R.drawable.mist_icon

        EnumWeatherDescriptionType.LIGHT_INTENSITY_DRIZZLE.description,
        EnumWeatherDescriptionType.DRIZZLE.description,
        EnumWeatherDescriptionType.HEAVY_INTENSITY_DRIZZLE.description,
        EnumWeatherDescriptionType.LIGHT_INTENSITY_DRIZZLE_RAIN.description,
        EnumWeatherDescriptionType.DRIZZLE_RAIN.description -> R.drawable.cloud_drizzle_icon

        EnumWeatherDescriptionType.HEAVY_INTENSITY_DRIZZLE_RAIN.description,
        EnumWeatherDescriptionType.SHOWER_RAIN_AND_DRIZZLE.description,
        EnumWeatherDescriptionType.HEAVY_SHOWER_RAIN_AND_DRIZZLE.description,
        EnumWeatherDescriptionType.SHOWER_DRIZZLE.description -> R.drawable.rain_icon
        else -> R.drawable.weather_sunny_icon
    }