package com.example.weatherapp.main_page.api

import com.example.weatherapp.main_page.api.models.WeatherDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET("data/2.5/weather")
    suspend fun getWeatherData(
        @Query("q") cityName: String,
        @Query("appid") key: String
    ): WeatherDataResponse
}