package com.example.weatherapp.main_page.ui

import com.example.weatherapp.common.mvp.BasePresenter
import com.example.weatherapp.main_page.interactor.MainPageInteractor
import com.example.weatherapp.main_page.model.WeatherData
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import timber.log.Timber

class MainPagePresenter(
    private val interactor: MainPageInteractor
) : BasePresenter<WeatherContract.View>(), WeatherContract.Presenter {
    private var weatherData: WeatherData? = null

    override fun getWeatherData(city: String, key: String) {
        launch {
            try {
                view?.showLoading(true)
                val weatherData = interactor.getWeatherData(city, key)
                view?.showWeatherData(data = weatherData)
                view?.changeIcon(data = weatherData)
            } catch (t: Throwable) {
                Timber.e("Get Data Error: ${t.message}")
            } catch (e: CancellationException) {
                Timber.e("Get Data coroutines Error: ${e.message}")
            } finally {
                view?.showLoading(false)
            }
        }
    }

    override fun transferWeatherData(): WeatherData? {
        if (weatherData == null) {
            return null
        }

        return weatherData
    }
}
