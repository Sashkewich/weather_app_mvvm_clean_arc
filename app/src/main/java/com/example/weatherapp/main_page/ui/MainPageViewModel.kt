package com.example.weatherapp.main_page.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherapp.main_page.interactor.MainPageInteractor
import com.example.weatherapp.main_page.model.WeatherData
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import timber.log.Timber

class MainPageViewModel(
    private val interactor: MainPageInteractor
) : ViewModel() {
    val weatherLiveData = MutableLiveData<WeatherData>()
    val isLoading = MutableLiveData<Boolean>()

    fun getWeatherData(cityName: String, key: String) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val weatherData = interactor.getWeatherData(cityName, key)
                weatherLiveData.value = weatherData
            } catch (t: Throwable) {
                Timber.e(t.message)
            } catch (e: CancellationException) {
                Timber.e(e.message)
            }finally {
                isLoading.value = false
            }
        }
    }
}