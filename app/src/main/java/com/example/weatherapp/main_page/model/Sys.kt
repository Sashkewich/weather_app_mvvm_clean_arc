package com.example.weatherapp.main_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sys(
    var country: String?,
    val sunrise : Long?,
    val sunset : Long?,
) : Parcelable
