package com.example.weatherapp.main_page.api.models

data class WindResponse(
    var speed : Double?
)
