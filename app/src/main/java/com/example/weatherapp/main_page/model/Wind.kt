package com.example.weatherapp.main_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Wind(
    var speed : Double?
) : Parcelable
