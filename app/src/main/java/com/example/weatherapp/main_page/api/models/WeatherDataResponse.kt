package com.example.weatherapp.main_page.api.models

data class WeatherDataResponse(
    var main: MainResponse?,
    var wind: WindResponse?,
    var sys: SysResponse?,
    var name: String?,
    var weather: List<WeatherResponse>?,
    var visibility: Int?
)
