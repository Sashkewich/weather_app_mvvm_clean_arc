package com.example.weatherapp.main_page.di

import com.example.weatherapp.common.di.InjectionModule
import com.example.weatherapp.main_page.api.WeatherApi
import com.example.weatherapp.main_page.interactor.MainPageInteractor
import com.example.weatherapp.main_page.repository.MainPageRemoteRepository
import com.example.weatherapp.main_page.repository.MainPageRepository
import com.example.weatherapp.main_page.ui.MainPagePresenter
import com.example.weatherapp.main_page.ui.MainPageViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object MainPageModule : InjectionModule {

    override fun create() = module {
        single { get<Retrofit>().create(WeatherApi::class.java) } //private val api: WeatherApi = RetrofitClient.getClient().create(WeatherApi::class.java)
        single<MainPageRepository> { MainPageRemoteRepository(get()) } //-> creating Interface implementation
        single { MainPageRemoteRepository(get()) } bind MainPageRepository::class //private val remoteRepository = MainPageRemoteRepository(api)
        factory { MainPageInteractor(get()) } //private val interactor = MainPageInteractor(remoteRepository)

        viewModelOf(::MainPagePresenter) // override val presenter: MainPagePresenter = MainPagePresenter(interactor)
        viewModelOf(::MainPageViewModel)
    }
}
