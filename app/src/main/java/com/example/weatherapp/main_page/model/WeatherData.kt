package com.example.weatherapp.main_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class WeatherData(
    var main: Main?,
    var wind: Wind?,
    var sys: Sys?,
    var name: String?,
    var weather: List<Weather>?,
    var visibility: Int?
) : Parcelable
