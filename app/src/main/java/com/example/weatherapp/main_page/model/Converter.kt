package com.example.weatherapp.main_page.model

import com.example.weatherapp.main_page.api.models.*

object Converter {
    fun fromNetwork(response: WeatherDataResponse): WeatherData {
        return WeatherData(
            main = response.main?.let { fromNetwork(it) },
            wind = response.wind?.let { fromNetwork(it) },
            sys = response.sys?.let { fromNetwork(it) },
            name = response.name,
            weather = response.weather?.let { fromNetwork(it) },
            visibility = response.visibility,
        )
    }

    private fun fromNetwork(response: MainResponse): Main {
        return Main(
            temp = response.temp,
            humidity = response.humidity,
        )
    }

    private fun fromNetwork(response: SysResponse): Sys {
        return Sys(
            country = response.country,
            sunrise = response.sunrise,
            sunset = response.sunset
        )
    }

    private fun fromNetwork(response: List<WeatherResponse>): List<Weather> {
        return response.map { data ->
            Weather(
                main = data.main,
                description = data.description,
                icon = data.icon
            )
        }
    }

    private fun fromNetwork(response: WindResponse): Wind {
        return Wind(
            speed = response.speed,
        )
    }
}