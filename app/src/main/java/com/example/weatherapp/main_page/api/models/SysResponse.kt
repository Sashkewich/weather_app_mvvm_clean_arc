package com.example.weatherapp.main_page.api.models

data class SysResponse(
    var country: String?,
    val sunrise : Long?,
    val sunset : Long?,
)
