package com.example.weatherapp.main_page.api.models

data class MainResponse(
    var humidity: Int?,
    var temp: Float?
    )

