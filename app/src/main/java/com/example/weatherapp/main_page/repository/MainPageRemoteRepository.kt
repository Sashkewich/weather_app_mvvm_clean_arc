package com.example.weatherapp.main_page.repository

import com.example.weatherapp.main_page.api.WeatherApi
import com.example.weatherapp.main_page.model.Converter

class MainPageRemoteRepository(
    val api: WeatherApi
) : MainPageRepository {
    override suspend fun getWeatherData(cityName: String, key: String) =
        Converter.fromNetwork(
            api.getWeatherData(
                cityName = cityName,
                key = key
            )
        )
}