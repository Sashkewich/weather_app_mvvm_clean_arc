package com.example.weatherapp.main_page.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.example.weatherapp.R
import com.example.weatherapp.common.mvp.BaseFragment
import com.example.weatherapp.databinding.FragmentMainBinding
import com.example.weatherapp.details.ui.DetailsFragment
import com.example.weatherapp.main_page.model.WeatherData
import com.example.weatherapp.utils.extensions.replace
import com.example.weatherapp.utils.extensions.viewbinding.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

private const val KEY = "2cb6cb1fd4ebd0d67addeeb82e919110"

class WeatherFragment : BaseFragment(R.layout.fragment_main) {

    private val binding: FragmentMainBinding by viewBinding()
    private val viewModel: MainPageViewModel by viewModel()

    @SuppressLint("SuspiciousIndentation")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.weatherLiveData.observe(viewLifecycleOwner) { weatherData ->
            showWeather(weatherData)
            showIcon(weatherData)
        }

        with(binding) {
            viewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
                progress.isVisible = isLoading
            }

            searchWeatherOfCity.doAfterTextChanged {
                val city = it.toString()
                viewModel.getWeatherData(city, KEY)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showWeather(data: WeatherData) {
        with(binding) {
            this.cityName.text =
                "${data.name?.replaceFirstChar { it.uppercase() }}, ${data.sys?.country}"
            this.tempValue.text = "${data.main?.temp?.minus(273.15)?.toInt()}°С"
            this.weatherSmallText.text =
                data.weather?.get(0)?.description?.replaceFirstChar { it.uppercase() }
            buttonMoreInfo.setOnClickListener {
                replace(DetailsFragment.newInstance(data))
            }
        }
    }

    private fun showIcon(data: WeatherData) {
        with(binding) {
            when (data.weather?.get(0)?.description.toString()) {
                "clear sky", "ясно" -> weatherIcon.setImageResource(R.drawable.weather_sunny_icon)

                "few clouds", "небольшая облачность" -> weatherIcon.setImageResource(R.drawable.weather_few_clouds_icon)

                "scattered clouds", "broken clouds", "overcast clouds" -> weatherIcon.setImageResource(
                    R.drawable.overcast_clouds_icon
                )

                "light rain" -> weatherIcon.setImageResource(R.drawable.light_rain_icon)

                "moderate rain", "heavy intensity rain", "very heavy rain", "extreme rain", "freezing rain", "light intensity shower rain",
                "shower rain", "heavy intensity shower rain", "ragged shower rain" -> weatherIcon.setImageResource(
                    R.drawable.rain_icon
                )

                "thunderstorm with light rain", "thunderstorm with rain", "thunderstorm with heavy rain", "light thunderstorm",
                "thunderstorm", "heavy thunderstorm", "ragged thunderstorm", "thunderstorm with light drizzle", "thunderstorm with drizzle",
                "thunderstorm with heavy drizzle" -> weatherIcon.setImageResource(R.drawable.thunderstorm_icon)

                "light shower snow", "light snow", "snow", "heavy snow", "sleet", "light shower sleet", "shower sleet",
                "light rain and snow", "rain and snow", "shower snow", "heavy shower snow" -> weatherIcon.setImageResource(
                    R.drawable.snow_icon
                )

                "mist", "fog" -> weatherIcon.setImageResource(R.drawable.mist_icon)

                "light intensity drizzle", "drizzle", "heavy intensity drizzle", "light intensity drizzle rain",
                "drizzle rain", "heavy intensity drizzle rain", "shower rain and drizzle", "heavy shower rain and drizzle",
                "shower drizzle" -> weatherIcon.setImageResource(R.drawable.cloud_drizzle_icon)
            }
        }
    }
}