package com.example.weatherapp.main_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Main(
    var humidity: Int?,
    var temp: Float?
    ) : Parcelable

