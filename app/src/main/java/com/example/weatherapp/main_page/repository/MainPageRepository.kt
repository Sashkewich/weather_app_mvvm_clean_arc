package com.example.weatherapp.main_page.repository

import com.example.weatherapp.main_page.model.WeatherData

interface MainPageRepository {
    suspend fun getWeatherData(cityName: String, key: String): WeatherData
}