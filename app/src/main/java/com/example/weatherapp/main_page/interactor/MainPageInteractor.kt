package com.example.weatherapp.main_page.interactor

import com.example.weatherapp.main_page.repository.MainPageRemoteRepository
import com.example.weatherapp.main_page.model.WeatherData

class MainPageInteractor(
    private val remoteRepository: MainPageRemoteRepository
) {
    suspend fun getWeatherData(city: String, key: String): WeatherData {
        return remoteRepository.getWeatherData(city,key)
    }
}