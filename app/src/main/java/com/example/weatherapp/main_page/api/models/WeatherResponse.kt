package com.example.weatherapp.main_page.api.models

data class WeatherResponse(
    val main: String?,
    val description: String?,
    val icon: String?
)