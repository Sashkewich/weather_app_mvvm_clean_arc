package com.example.weatherapp.main_page.ui

import androidx.annotation.StringRes
import com.example.weatherapp.common.mvp.MvpPresenter
import com.example.weatherapp.common.mvp.MvpView
import com.example.weatherapp.main_page.model.WeatherData

interface WeatherContract {

    interface View : MvpView {
        fun showWeatherData(data: WeatherData)
        fun changeIcon(data: WeatherData)
        fun showErrorMessage(t: Throwable)
        fun showLoading(isLoading: Boolean)
    }

    interface Presenter : MvpPresenter<View> {
        fun getWeatherData(city: String, key: String)
        fun transferWeatherData(): WeatherData?
    }
}
