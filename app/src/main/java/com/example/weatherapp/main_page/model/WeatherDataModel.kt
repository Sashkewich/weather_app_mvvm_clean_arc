package com.example.weatherapp.main_page.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class WeatherDataModel: ViewModel(){
    val weatherData:MutableLiveData<WeatherData> by lazy {
        MutableLiveData<WeatherData>()
    }
}